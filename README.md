## Media Player Experiment C523 

Renew : 5 Oct 2021

### Project Info
Case No         : C523

Project Name    : Media Player Experiment 

Project Status  : *Currently is in experimental stage*

To Do: 
1. Need to add seek bar for media player
2. need to add playlist for media player 
3. Enable background service 
4. Notification bar implementation

![screenshot](main_screen.PNG)

<hr>

**Farhan Sadik**
